package com.intelliseq.springexamples.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HtmController {
	
	public HtmController() {
//		ApplicationContext context = new ClassPathXmlApplicationContext(new String[] {
//				"classpath:/org/cremag/seQinspector/application-config.xml"});
	}
	
	@RequestMapping("/home.htm")
	public ModelAndView homeRequest(HttpServletRequest request,
			HttpServletResponse response) throws Exception {

			ModelAndView mav = new ModelAndView("home");
			return mav;
	}
}